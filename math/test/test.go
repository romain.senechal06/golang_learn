package main

import (
	"fmt"
	"twg/math"
)

func main() {

	add := math.Add(10, 15)
	if add != 25 {
		msg := fmt.Sprintf("On attend 25 mais le résultat est %d", add)
		panic("ERREUR Add : " + msg)
	}
	fmt.Println("Test pass")

}

package main

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func LoggingTest(t *testing.T) {
	t.Log("Here is a test")
	// t.FailNow()
	// t.Fail()
	t.Logf("The number to format is %d", 555)
	t.Error("Error = log + fail")
	t.Fatal("Stop test execution with message")
}

func TestHandeler(t *testing.T) {
	w := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "", nil)

	if err != nil {
		t.Fatalf("http.NewRequest() err : %s", err)
	}

	Handler(w, r)

	resp := w.Result()
	if resp.StatusCode != 200 {
		t.Fatalf("Handler() Status = %d; want %d", resp.StatusCode, 200)
	}

	contentType := resp.Header.Get("Content-Type")
	if contentType != "application/json" {
		t.Errorf("Handler() Content-Type = %q; want %q", contentType, "application/json")
	}

	data, err := io.ReadAll(resp.Body)
	// fmt.Printf("data is %s :", data)
	if err != nil {
		t.Fatalf("io.ReadAll(response.Body) err = %s", err)
	}

	var p Person
	err = json.Unmarshal(data, &p)
	if err != nil {
		t.Fatalf("json.Unmarshal(data) err = %s", err)
	}

	wantAge := 29
	if p.Age != wantAge {
		t.Errorf("person.Age = %d; want %d", p.Age, wantAge)
	}

	wantName := "Bob"
	if p.Name != wantName {
		t.Errorf("person.Name = %s; want %s", p.Name, wantName)
	}

}

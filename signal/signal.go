package main

import (
	"encoding/json"
	"net/http"
)
//Person age
type Person struct {
	Age        int
	Name       string
	Occupation string
}

func Handler(w http.ResponseWriter, r *http.Request) {

	p := Person{
		Age:        29,
		Name:       "Bob",
		Occupation: "Plumber",
	}

	data, err := json.Marshal(p)

	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(200)
	w.Write(data)
}

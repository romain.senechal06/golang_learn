package example

import "fmt"

//Simple example test with output
func ExampleHello() {
	greeting := Hello("Bob")
	fmt.Println(greeting)

	// Output: Hello Bob
}

//Unordered example test
func ExamplePage() {
	checkins := map[string]bool{
		"Bob":     true,
		"Alice":   false,
		"John":    false,
		"Maurice": true,
		"Janette": true,
		"Arthur":  false,
	}

	Page(checkins)

	//Unordered Output:
	//Paging Alice; please see the front desk to ckeck in
	//Paging John; please see the front desk to ckeck in
	//Paging Arthur; please see the front desk to ckeck in

}

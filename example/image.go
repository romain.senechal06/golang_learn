package example

import "io"

//Image is a fake image type
type Image struct{}

//Decode is a fake function to decode image provided via an io.Reader
func Decode(r io.Reader) (*Image, error) {
	//Use std lib's image package to read image an dparse local image
	// type ...
	return &Image{}, nil
}

// Crop is a fake function to crop images
func Crop(img *Image, x1, y1, x2, y2 int) error {
	return nil
}

//Encode is a fake function to encode image
func Encode(img *Image, format string, w io.Writer) error {
	//Use io.Writer to decode image
	return nil
}
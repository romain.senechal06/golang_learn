// Example is a package for demonstrating eamaple in source code.
package example

import "fmt"

type Demo struct{}

func (d Demo) DemoHello() {}

//Hello print to the person provided
func Hello(name string) string{
	return fmt.Sprintf("Hello %s", name)
}

//Page will print out a message asking each person  who hasn't check in 
//to do so.
func Page(checkIns map[string]bool) {
	for name, checkIns := range checkIns {
		if !checkIns {
			fmt.Printf("Paging %s; please see the front desk to ckeck in\n", name)
		}
	}
}